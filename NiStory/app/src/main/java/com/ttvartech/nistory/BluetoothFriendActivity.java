package com.ttvartech.nistory;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class BluetoothFriendActivity extends AppCompatActivity {

    BluetoothAdapter mBluetoothAdapter;
    AcceptThread acceptThread;
    CommunicationThread communicationThread;

    private boolean BLUETOOTH_IS_ENABLED = false;
    private int REQUEST_ENABLE_BT = 1;
    static final int PERMISSION_REQUEST_LOCATION = 2;

    private static final String APP_NAME = "NiStory";
    private static final UUID MY_UUID = UUID.fromString("7b306be7-385d-4157-980c-ed92d80366ab");

    ArrayList<BluetoothDevice> bluetoothDeviceArrayList;
    ArrayList<String> stringBluetoothDeviceNameArrayList;
    ArrayAdapter<String> stringBluetoothDeviceNameArrayAdapter;
    ListView bluetoothDeviceListView;

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_friend);

        bluetoothSetup();

        // Register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        bluetoothDeviceListView = findViewById(R.id.bluetooth_devices_list_view);
        bluetoothDeviceArrayList = new ArrayList<>();
        stringBluetoothDeviceNameArrayList = new ArrayList<>();
        stringBluetoothDeviceNameArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, stringBluetoothDeviceNameArrayList);

        bluetoothDeviceListView.setAdapter(stringBluetoothDeviceNameArrayAdapter);
        bluetoothDeviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // selected item
                String selectedBluetoothDeviceName = stringBluetoothDeviceNameArrayAdapter.getItem(position);

                Toast toast = Toast.makeText(getApplicationContext(), selectedBluetoothDeviceName, Toast.LENGTH_SHORT);
                toast.show();

                ConnectThread connectThread = new ConnectThread(bluetoothDeviceArrayList.get(position));
                connectThread.start();
            }
        });

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

    }

    private void bluetoothSetup() {

        requestLocationPermission();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(BluetoothFriendActivity.this, "Bluetooth is not supported", Toast.LENGTH_SHORT).show();
            finish();
        } else {

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                BLUETOOTH_IS_ENABLED = true;
                makeDeviceDiscoverable();
            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {

                makeDeviceDiscoverable();

                BLUETOOTH_IS_ENABLED = true;
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                BLUETOOTH_IS_ENABLED = false;
            }
        }
    }//onActivityResult

    private void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(BluetoothFriendActivity.this, "Location permission granted!", Toast.LENGTH_SHORT).show();

                } else {
                    requestLocationPermission();
                }
            }
        }
    }

    private void makeDeviceDiscoverable() {

        if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 600);
            startActivity(discoverableIntent);
        }

        // TODO: 5/24/2018

        acceptThread = new AcceptThread();
        acceptThread.start();

    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();

                stringBluetoothDeviceNameArrayAdapter.add(deviceName);
                bluetoothDeviceArrayList.add(device);

            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(mReceiver);

        if (acceptThread != null)
            acceptThread.cancel();

        mBluetoothAdapter.disable();
    }

    public void scanForBluetoothAction(View view) {

        if (!BLUETOOTH_IS_ENABLED) {
            bluetoothSetup();
        } else {
            mBluetoothAdapter.startDiscovery();
            stringBluetoothDeviceNameArrayAdapter.clear();
            stringBluetoothDeviceNameArrayList.clear();
        }

    }


    public void sendAction(View view) {

        String myUID = mAuth.getUid();
        communicationThread.write(myUID.getBytes());

    }


    private class AcceptThread extends Thread {

        private String TAG = "AcceptThread.TAG";

        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket's listen() method failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {

                    socket = mmServerSocket.accept();

                } catch (IOException e) {
                    Log.e(TAG, "Socket's accept() method failed", e);
                    break;
                }

                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
                    //manageMyConnectedSocket(socket);

                    communicationThread = new CommunicationThread(socket);
                    communicationThread.start();

                    Message message = Message.obtain();
                    message.what = 100;
                    handler.sendMessage(message);

                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }


    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        private String TAG = "ConnectThread.TAG";

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery(); // TODO add cancelDiscovery() to AcceptThread

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();

                communicationThread = new CommunicationThread(mmSocket);
                communicationThread.start();

                Message message = Message.obtain();
                message.what = 200;
                handler.sendMessage(message);

            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            //manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }


    private class CommunicationThread extends Thread {

        private String TAG = "CommunicationThread.TAG";

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public CommunicationThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                    // Send the obtained bytes to the UI activity.
                    Message readMsg = handler.obtainMessage(
                            300, numBytes, -1,
                            mmBuffer);
                    readMsg.sendToTarget();

                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);

                // Share the sent message with the UI activity.
                //Message writtenMsg = handler.obtainMessage(400, -1, -1, mmBuffer);
                //writtenMsg.sendToTarget();

            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                // Send a failure message back to the activity.
                Message writeErrorMsg =
                        handler.obtainMessage(500);
                Bundle bundle = new Bundle();
                bundle.putString("toast",
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                handler.sendMessage(writeErrorMsg);
            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }


    Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case 100: {
                    Toast.makeText(BluetoothFriendActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                    communicationThread.write(AuthorizationManager.getUserUid().getBytes());

                    //stringBluetoothDeviceNameArrayAdapter.clear();
                    //stringBluetoothDeviceNameArrayList.clear();
                }
                break;
                case 200: {
                    Toast.makeText(BluetoothFriendActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                    communicationThread.write(AuthorizationManager.getUserUid().getBytes());

                    //stringBluetoothDeviceNameArrayAdapter.clear();
                    //stringBluetoothDeviceNameArrayList.clear();
                }
                break;
                case 300: {
                    byte[] readBuff = (byte[]) msg.obj;
                    String friendUId = new String(readBuff, 0, msg.arg1);

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("friends").child(friendUId).setValue(friendUId);


                    Intent intent = new Intent(BluetoothFriendActivity.this, SignInActivity.class);
                    startActivity(intent);
                    BluetoothFriendActivity.this.finish();

                }
                break;
                case 400: {
                    Toast.makeText(BluetoothFriendActivity.this, "4040404040", Toast.LENGTH_SHORT).show();
                }
                break;
                case 500: {
                    Toast.makeText(BluetoothFriendActivity.this, "5050505050", Toast.LENGTH_SHORT).show();
                }
                break;
            }

            return false;
        }
    });

}
