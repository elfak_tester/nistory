package com.ttvartech.nistory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {

    private ViewGroup mParent;

    private static ArrayList<User> mFriendArrayList;

    public FriendAdapter(ArrayList<User> friendArrayList) {
        this.mFriendArrayList = friendArrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mParent = parent;
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_view_holder_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide.with(mParent.getContext()).load(mFriendArrayList.get(position).getmUserImageUrl()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.user_no_image)).into(holder.mImageButtonFriendImage);
        holder.mTextViewFriendName.setText(mFriendArrayList.get(position).getmUserFirstName() + " " + mFriendArrayList.get(position).getmUserLastName());
        holder.mTextViewFriendPoints.setText(String.valueOf(mFriendArrayList.get(position).getmUserPoints()));

    }

    @Override
    public int getItemCount() {
        return mFriendArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout mRelativeLayout;

        private ImageView mImageButtonFriendImage;
        private TextView mTextViewFriendName;
        private TextView mTextViewFriendPoints;

        public ViewHolder(RelativeLayout itemView) {
            super(itemView);

            mRelativeLayout = itemView;

            mImageButtonFriendImage = itemView.findViewById(R.id.friend_image_image_button);
            mTextViewFriendName = itemView.findViewById(R.id.friend_name_text_view);
            mTextViewFriendPoints = itemView.findViewById(R.id.friend_points_text_view);

        }

    }

}
