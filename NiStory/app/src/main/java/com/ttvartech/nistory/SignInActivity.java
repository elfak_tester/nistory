package com.ttvartech.nistory;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ttvartech.nistory.Util.AuthorizationManager;

public class SignInActivity extends AppCompatActivity {

    //Sign up
    private EditText mUserSignInEmailEditText;
    private EditText mUserSignInPasswordEditText;


    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getSupportActionBar().hide();

        //Finding views in activity_sign_up
        mUserSignInEmailEditText = findViewById(R.id.user_sign_in_email_auto_complete_text_view);
        mUserSignInPasswordEditText = findViewById(R.id.user_sign_in_password_edit_text);

        //Firebase
        mAuth = FirebaseAuth.getInstance();

        registredUser();

    }

    private void registredUser() {
        if (AuthorizationManager.getUserName().isEmpty() &&
                AuthorizationManager.getUserPassword().isEmpty())
            return;

        mAuth.signInWithEmailAndPassword(AuthorizationManager.getUserName(), AuthorizationManager.getUserPassword())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = mAuth.getCurrentUser();

                            currentUser = mAuth.getCurrentUser();
                            AuthorizationManager.setUserUid(currentUser.getUid());


                            //Toast.makeText(SignInActivity.this, "Authentication succeeded.", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    public void openUserForgotPasswordActivity(View view) {

    }

    public void tryToSignIn(View view) {

        String email = mUserSignInEmailEditText.getText().toString();
        String password = mUserSignInPasswordEditText.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = mAuth.getCurrentUser();

                            currentUser = mAuth.getCurrentUser();
                            AuthorizationManager.setUserUid(currentUser.getUid());

                            Toast.makeText(SignInActivity.this, "Authentication succeeded.",
                                    Toast.LENGTH_SHORT).show();

                            AuthorizationManager.setUserName(email);
                            AuthorizationManager.setUserPassword(password);

                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {

                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

    public void openUserSignUpActivity(View view) {
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
//        currentUser = mAuth.getCurrentUser();
//
//        if (currentUser != null) {
//            UserToken.USER_UID = currentUser.getUid();
//
//            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//            startActivity(intent);
//            finish();
//        } else
//            return;
    }

}

//ovaj SHI-1 vise ne vazi zbog toga mapa nije radila :(
//A3:58:F5:B0:3D:31:6D:56:B1:A7:06:8E:20:A0:D5:A5:49:4A:C1:FB
//30:8A:B5:E5:69:E6:8B:8A:D6:ED:02:14:2A:6B:04:10:DF:30:9A:8A