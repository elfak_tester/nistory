package com.ttvartech.nistory;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class MonumentAdapter extends RecyclerView.Adapter<MonumentAdapter.ViewHolder> {

    private ViewGroup mParent;
    private Listener listener;

    private static ArrayList<Monument> mMonumentsArrayList;
    private ArrayList<String> monumentsForCurrentUser;

    public MonumentAdapter(ArrayList<Monument> monumentArrayList, ArrayList<String> monumentsForCurrentUser, Listener listener) {
        this.mMonumentsArrayList = monumentArrayList;
        this.listener = listener;
        this.monumentsForCurrentUser = monumentsForCurrentUser;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mParent = parent;
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.monument_view_holder_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(mParent.getContext()).load(mMonumentsArrayList.get(position).getmMonumentImageUrl_1()).into(holder.mImageViewMonumentImage);
        holder.mImageViewMonumentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null && !mMonumentsArrayList.isEmpty()) {
                    listener.onItemClick(mMonumentsArrayList.get(holder.getAdapterPosition()));
                }
            }
        });
        holder.mTextViewMonumentName.setText(mMonumentsArrayList.get(position).getmMonumentName());
        holder.mTextViewMonumentDescription.setText(mMonumentsArrayList.get(position).getmMonumentDescription());
        holder.mTextViewMonumentHearts.setText(String.valueOf(mMonumentsArrayList.get(position).getmMonumentHearts()));
        //holder.mTextViewMonumentDistance.setText(String.valueOf(mMonumentsArrayList.get(position).getmMonumentLatitude()));

        boolean isVisited = false;
        for (String mon : monumentsForCurrentUser) {
            if (mon.equals(mMonumentsArrayList.get(position).getmMonumentId()))
                isVisited = true;
        }
        if (isVisited) {
            holder.mImageButtonMonumentHeart.setImageDrawable(holder.mConstraintLayout.getContext().getResources().getDrawable(R.drawable.heart_full_color));

        } else {
            holder.mImageButtonMonumentHeart.setImageDrawable(holder.mConstraintLayout.getContext().getResources().getDrawable(R.drawable.outline_favorite_border_black_24));
        }


        holder.mImageButtonMonumentHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.mImageButtonMonumentHeart.setImageDrawable(holder.mConstraintLayout.getContext().getResources().getDrawable(R.drawable.ic_baseline_favorite_red_24px));

                boolean isVisited = false;
                for (String mon : monumentsForCurrentUser) {
                    if (mon.equals(mMonumentsArrayList.get(position).getmMonumentId()))
                        isVisited = true;
                }

                if (!mMonumentsArrayList.isEmpty()) {

                    if (isVisited) {
                        listener.dislike(mMonumentsArrayList.get(position));
                    } else {
                        listener.like(mMonumentsArrayList.get(position));
                    }
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mMonumentsArrayList.size();
    }


    public interface Listener {
        void onItemClick(Monument monument);

        void like(Monument monument);

        void dislike(Monument monument);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout mConstraintLayout;

        private ImageButton mImageButtonMonumentHeart;
        private TextView mTextViewMonumentName;
        private TextView mTextViewMonumentDescription;
        private TextView mTextViewMonumentDistance;
        private ImageView mImageViewMonumentImage;
        private TextView mTextViewMonumentHearts;

        public ViewHolder(ConstraintLayout itemView) {
            super(itemView);

            mConstraintLayout = itemView;

            mImageButtonMonumentHeart = itemView.findViewById(R.id.list_row_monument_heart_image_button);
//            mImageButtonMonumentHeart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                }
//            });
            mTextViewMonumentName = itemView.findViewById(R.id.list_row_monument_name_text);
            mTextViewMonumentDescription = itemView.findViewById(R.id.list_row_monument_description_text);
           // mTextViewMonumentDistance = itemView.findViewById(R.id.list_row_overlay_monumnent_distance_text_view);
            mImageViewMonumentImage = itemView.findViewById(R.id.list_row_monument_image_image_view);
            mTextViewMonumentHearts = itemView.findViewById(R.id.list_row_monument_hearts);


        }

    }


}
