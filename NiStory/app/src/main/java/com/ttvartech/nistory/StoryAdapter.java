package com.ttvartech.nistory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {

    private ViewGroup mParent;
    private Listener listener;
    private ViewHolder updateHolder;
    private static ArrayList<Story> mStoryArrayList;
    private static ArrayList<String> mLikedStoriesForSpecificUserArrayList;

    public StoryAdapter(ArrayList<Story> storyArrayList, ArrayList<String> mLikedStoriesForSpecificUserArrayList, Listener listener) {
        this.mStoryArrayList = storyArrayList;
        this.mLikedStoriesForSpecificUserArrayList = mLikedStoriesForSpecificUserArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mParent = parent;
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.story_view_holder_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String data = dataFormat(mStoryArrayList.get(position).getmStoryTime());

        Glide.with(mParent.getContext()).load(mStoryArrayList.get(position).getmStoryUserImageUrl()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.user_no_image)).into(holder.mImageButtonStoryUserPhoto);
        holder.mTextViewStoryUserName.setText(mStoryArrayList.get(position).getmStoryUserName());
        holder.mTextViewStoryTime.setText(data);
        holder.mTextViewStoryText.setText(mStoryArrayList.get(position).getmStoryDescription());
        Glide.with(mParent.getContext()).load(mStoryArrayList.get(position).getmStoryImageUrl()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.happy_placeholder)).into(holder.mImageViewStoryImage);
        holder.mImageViewStoryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(mStoryArrayList.get(position));
            }
        });
        holder.mTextViewStoryHearts.setText(String.valueOf(mStoryArrayList.get(position).getmStoryHearts()));

        boolean isLiked = false;
        for (String story : mLikedStoriesForSpecificUserArrayList) {
            if (story.equals(mStoryArrayList.get(position).getmStoryID()))
                isLiked = true;
        }

        if (isLiked) {
            holder.mImageButtonStoryHeart.setBackgroundResource(R.drawable.heart_full_color);
        } else {
            holder.mImageButtonStoryHeart.setBackgroundResource(R.drawable.outline_favorite_border_black_24);
        }

        holder.mImageButtonStoryHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateHolder = holder;
                boolean isLiked = false;
                for (String story : mLikedStoriesForSpecificUserArrayList) {
                    if (story.equals(mStoryArrayList.get(position).getmStoryID()))
                        isLiked = true;
                }

                if (isLiked) {
                    listener.dislike(mStoryArrayList.get(position), position);
                } else {
                    listener.like(mStoryArrayList.get(position), position);
                }

            }
        });

    }

    public static final SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    private String dataFormat(String dataStr){
        String niceDateStr = "";
        try {
            Date date = inputFormat.parse(dataStr);
            niceDateStr = (String) DateUtils.getRelativeTimeSpanString(date.getTime() , Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return niceDateStr;
    }

    @Override
    public int getItemCount() {
        return mStoryArrayList.size();
    }

    public interface Listener {
        void onItemClick(Story story);

        void like(Story story, int pos);

        void dislike(Story story, int pos);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mConstraintLayout;

        private ImageView mImageButtonStoryUserPhoto;
        private TextView mTextViewStoryUserName;
        private TextView mTextViewStoryTime;
        private TextView mTextViewStoryText;
        private ImageView mImageViewStoryImage;
        private TextView mTextViewStoryHearts;
        private ImageButton mImageButtonStoryHeart;

        public ViewHolder(LinearLayout itemView) {
            super(itemView);

            mConstraintLayout = itemView;

            mImageButtonStoryUserPhoto = itemView.findViewById(R.id.story_user_image_image_button);
            mTextViewStoryUserName = itemView.findViewById(R.id.story_user_name_text_view);
            mTextViewStoryTime = itemView.findViewById(R.id.story_time_text_view);
            mTextViewStoryText = itemView.findViewById(R.id.story_text_text_view);
            mImageViewStoryImage = itemView.findViewById(R.id.story_image_image_view);
            mTextViewStoryHearts = itemView.findViewById(R.id.story_hearts_text_view);
            mImageButtonStoryHeart = itemView.findViewById(R.id.story_heart_button);
        }

    }

}
