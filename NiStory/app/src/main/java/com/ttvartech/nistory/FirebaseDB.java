package com.ttvartech.nistory;

import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ttvartech.nistory.Util.AuthorizationManager;
import com.ttvartech.nistory.Util.NiStory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executor;

import static com.ttvartech.nistory.Util.NiStory.getContext;

public class FirebaseDB {

    private static FirebaseAuth mAuth;
    private static DatabaseReference mDatabaseRef;
    private static StorageReference mStorageRef;
    private FirebaseUser currentUser;
    private boolean isRegistred = false;


    static void updateUserPoints(int newPoints) {
        DatabaseReference mDatabaseRef = com.google.firebase.database.FirebaseDatabase.getInstance().getReference();
        mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                int oldPoints = user.getmUserPoints();

                mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserPoints").setValue(oldPoints + newPoints);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void addMonuments()
    {

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();

//43.332792,21.8935523,3
        //  Monument monument = new Monument("Dobrila Stambolic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fkonstantin_veliki_one.jpg?alt=media&token=679fdacc-831a-4894-a509-7ea02ca27c9f", 0, 43.332792, 21.8935523);
        //  mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);

//        Monument monument = new Monument("Valerije1", "Lorem ipsum", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/user_profile_photos%2F1re1JK7FEve42Hl493jUVgGXm5i1?alt=media&token=d9abea8f-2ec6-444d-82bc-045383ad4697", 2, 43.333033, 21.892597);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Valerije2", "Lorem ipsum", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/user_profile_photos%2F1re1JK7FEve42Hl493jUVgGXm5i1?alt=media&token=d9abea8f-2ec6-444d-82bc-045383ad4697", 2, 43.333033, 21.892597);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Valerije3", "Lorem ipsum", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/user_profile_photos%2F1re1JK7FEve42Hl493jUVgGXm5i1?alt=media&token=d9abea8f-2ec6-444d-82bc-045383ad4697", 2, 43.333033, 21.892597);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Valerije4", "Lorem ipsum", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/user_profile_photos%2F1re1JK7FEve42Hl493jUVgGXm5i1?alt=media&token=d9abea8f-2ec6-444d-82bc-045383ad4697", 2, 43.333033, 21.892597);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        Monument monument = new Monument("Constantine the Great", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fkonstantin_veliki_one.jpg?alt=media&token=679fdacc-831a-4894-a509-7ea02ca27c9f", 5, 43.322827, 21.896966);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);

//        monument = new Monument("Edict of Milan", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fmilanski_edikt_one.jpg?alt=media&token=77a033af-f5a9-4c74-8bde-be6b16cd010d", 2, 43.322827, 21.896966);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Serbian-German agreement", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fbarbarosa_one.jpg?alt=media&token=8be02814-d74a-4aae-ac4f-ae358bdacd68", 6, 43.322980, 21.894473);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Nis fortress", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Ftvrdjava_one.jpg?alt=media&token=f2926350-8640-4d11-8124-4674336edfe0", 7, 43.323360, 21.895385);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Karadjordje Djordje Petrovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fvozd_karadjordje_one.jpg?alt=media&token=0573c290-657a-4dab-9f5c-df68a592bc25", 3, 43.320362, 21.903673);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Monument to the Liberators of Nis", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Foslobodioci_nisa_one.jpg?alt=media&token=d14d5295-5d8f-45ae-8ee3-6b7bbda87a02", 4, 43.321231, 21.895806);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Monument for the hanged ones", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fspomenik_obesenima_one.jpg?alt=media&token=3549a241-a1a0-46f7-a7ec-2eac7017b54d", 1, 43.322263, 21.895326);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Monument to Milan Obrenovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fknezu_milanu_one.jpg?alt=media&token=739be60d-6813-4eea-b3be-1db9c17b297f", 8, 43.324472, 21.895095);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Stevan Sremac", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fstevan_sremac_one.jpg?alt=media&token=19dec78b-2cdd-4eaa-8cc6-bd90b8572f19", 6, 43.320315, 21.899846);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Sremac and Kalca", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fkalca_sremac_one.jpg?alt=media&token=241809a1-3575-4fb1-a491-f2141fe5883b", 6, 43.318137, 21.895317);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Officers Club", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Foficirski_dom_one.jpg?alt=media&token=be1ad127-cdf0-4680-a181-f3f177404424", 4, 43.323435, 21.898322);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Petar Bojovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fpetar_bojovic_one.jpg?alt=media&token=cb834bdd-769c-49f2-a16f-21d14122ff66", 5, 43.322338, 21.896166);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Filip Filipovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Ffilip_filipovic_one.jpg?alt=media&token=ac91b05d-5dfa-4f1b-80e5-e9382cc3520e", 7, 43.322566, 21.897512);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Ljubomir Jakovljevic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fljubomir_jakovljevic_one.jpg?alt=media&token=09c8170c-aba2-42c6-bb2f-91ffe61cd6c1", 4, 43.322699, 21.896964);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Aleksandar I Karadjordjevic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Faleksandar_one.jpg?alt=media&token=8c1a2f15-2e8d-4620-a64a-38b43fee5390", 9, 43.318161, 21.891072);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Filip Kljajic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Ffilip_kljajic_one.jpg?alt=media&token=be54d174-d1b3-4994-840a-ea8950c7909c", 4, 43.322633, 21.897184);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Miodrag Mija Stanimirovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fmiodrag_mija_one.jpg?alt=media&token=4d760583-d9e2-4493-847b-bfc3e45051fe", 7, 43.322862, 21.897361);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Attack on Germans", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fnapad_na_nemce_one.jpg?alt=media&token=b36bccb3-a71a-4ec5-8f27-5ed943c2da84", 4, 43.322721, 21.897488);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Sreten Mladenovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fsreten_mladenovic_one.jpg?alt=media&token=cc317988-bbc5-40cd-9a05-b0134c642cc9", 3, 43.322583, 21.897407);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Stanko Paunovic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fstanko_paunovic_one.jpg?alt=media&token=ea8795f2-1221-429a-bdef-fefd761b9343", 6, 43.322821, 21.897615);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Djurdjelina Djuka Dinic", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fdjuka_dinic_one.jpg?alt=media&token=d3a44b84-4988-49cb-be6d-34dda8844602", 9, 43.323423, 21.894252);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Konrad Zilnik Slobodan", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fkomrad_zilkin_one.jpg?alt=media&token=edd61882-026b-4184-accc-e0641b6c6a95", 5, 43.322721, 21.897698);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("The graves of Soviet soldiers", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fsovjetski_vojnici_one.jpg?alt=media&token=7f86b1d9-2249-4c25-b809-8f744c2838bb", 6, 43.318127, 21.890866);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Pane Djukic Limar", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fpane_djukic_one.jpg?alt=media&token=0cdb1f26-9ed6-4a63-9767-1e8a1f26d78a", 7, 43.320337, 21.903856);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("University of Nis building", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Funiverzitet_one.jpg?alt=media&token=ea8c6f4a-18dd-40ef-baa2-0cfd7823ab0c", 8, 43.323085, 21.894053);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Monument for the hanged ones", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fspomen_kapela_one.jpg?alt=media&token=4aa55b76-6239-4d17-af7f-14195d36ace3", 4, 43.323337, 21.894685);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
//
//        monument = new Monument("Victims of NATO aggression", "", "https://firebasestorage.googleapis.com/v0/b/nistory-8443b.appspot.com/o/monument_photos%2Fzrtve_natoa_one.jpg?alt=media&token=5e925a6f-f3bd-49be-ae72-33ead15b0ee9", 4, 43.323036, 21.894663);
//        mDatabaseRef.child("monuments/" + UUID.randomUUID()).setValue(monument);
    }

    static void updateMonuments(final Monument monument, String supported) {
        int heart = 0;

        if (supported.equals("like"))
            heart = monument.getmMonumentHearts() + 1;
        else
            heart = monument.getmMonumentHearts() - 1;

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        String key = ref.child("monuments").child(monument.getmMonumentId()).getKey();

        Monument mon = new Monument(monument.getmMonumentName(), monument.getmMonumentDescription(), monument.getmMonumentImageUrl_1(), heart, monument.getmMonumentLatitude(), monument.getmMonumentLongitude(), monument.getmMonumentQuestion(), monument.getmMonumentAnswer());
        Map<String, Object> postValues = mon.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/monuments/" + key, postValues);

        ref.updateChildren(childUpdates);
    }

    static void updateStories(Story story, String like, int pos) {

        int heart = 0;

        if (like.equals("like"))
            heart = story.getmStoryHearts() + 1;
        else
            heart = story.getmStoryHearts() - 1;

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        String key = ref.child("stories").child(story.getmStoryCreatorID()).child(story.getmStoryID()).getKey();
        ref.child("stories").child(story.getmStoryCreatorID()).child(story.getmStoryID()).child("mStoryHearts").setValue(heart);

        //mStoryAdapter.notifyItemChanged(pos);
    }

    static void getMonuments(ArrayList<Monument> monumentArrayList, ArrayList<Location> monumentLocations, MonumentAdapter mMonumentAdapter) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("monuments");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count ", "" + snapshot.getChildrenCount());
                monumentArrayList.clear();

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Location location = new Location("");
                    Monument monument = postSnapshot.getValue(Monument.class);
                    monument.setmMonumentId(postSnapshot.getKey());
                    monumentArrayList.add(monument);
                    location.setLatitude(monument.getmMonumentLatitude());
                    location.setLongitude(monument.getmMonumentLongitude());
                    monumentLocations.add(location);

                }
                mMonumentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static  void getMonumentsForSpecificUser(final ArrayList<String> monumentsForCurrentUser, MonumentAdapter mMonumentAdapter) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("monuments");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count ", "" + snapshot.getChildrenCount());
                monumentsForCurrentUser.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Location location = new Location("");
                    String monument = postSnapshot.getValue(String.class);
                    monumentsForCurrentUser.add(monument);
                }
                mMonumentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    static void addMonumentInUser(final Monument monument) {
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("monuments");
        ref.child(monument.getmMonumentId()).setValue(monument.getmMonumentId());
    }

    static void addUserInStory(final Story story, final int position, ArrayList<Story> storyArrayList) {
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("stories").child(story.getmStoryCreatorID()).child(story.getmStoryID()).child("users");
        ref.child(AuthorizationManager.getUserUid()).setValue(AuthorizationManager.getUserUid());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                storyArrayList.get(position).getmStoryUsers().clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String s = postSnapshot.getValue(String.class);
                    storyArrayList.get(position).getmStoryUsers().add(postSnapshot.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    static void deleteUserInStory(final Story story) {
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("stories").child(story.getmStoryCreatorID()).child(story.getmStoryID()).child("users");
        ref.child(AuthorizationManager.getUserUid()).setValue(null);
    }

    public static void deleteMonumentInUser(final Monument monument) {
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("monuments");
        ref.child(monument.getmMonumentId()).setValue(null);
    }


}
