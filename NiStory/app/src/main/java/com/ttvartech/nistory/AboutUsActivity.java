package com.ttvartech.nistory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Date;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView textView = findViewById(R.id.app_version);
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        textView.setText(buildDate.toString());

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
