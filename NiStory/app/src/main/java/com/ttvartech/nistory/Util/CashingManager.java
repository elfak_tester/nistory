package com.ttvartech.nistory.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ttvartech.nistory.Util.NiStory;

/**
 * Used for casing data
 */
public class CashingManager {
    public static final String TAG = CashingManager.class.getSimpleName();
    private static CashingManager instance;

    public static CashingManager load() {
        if (instance == null) {
            instance = new CashingManager();
        }
        return instance;
    }

    //region SharedPreferences setup
    private SharedPreferences.Editor prefStore() {
        Context context = NiStory.getContext();
        return PreferenceManager.getDefaultSharedPreferences(NiStory.getContext()).edit();
    }

    private SharedPreferences prefRead() {
        Context context = NiStory.getContext();
        return PreferenceManager.getDefaultSharedPreferences(NiStory.getContext());
    }
    //endregion

    //region Storing data
    public CashingManager store(@NonNull String key, Integer data) {
        prefStore().putInt(key, data).commit();
        return this;
    }

    public CashingManager store(@NonNull String key, Float data) {
        prefStore().putFloat(key, data).commit();
        return this;
    }

    public CashingManager store(@NonNull String key, Boolean data) {
        prefStore().putBoolean(key, data).commit();
        return this;
    }

    public CashingManager store(@NonNull String key, Long data) {
        prefStore().putLong(key, data).commit();
        return this;
    }

    public CashingManager store(@NonNull String key, String data) {
        prefStore().putString(key, data).commit();
        return this;
    }
    //endregion

    //region Reading data
    public Integer readInt(@NonNull String key) {
        return prefRead().getInt(key, -1);
    }

    public Boolean readBool(@NonNull String key) {
        return prefRead().getBoolean(key, false);
    }

    public Long readLong(@NonNull String key) {
        return prefRead().getLong(key, -1);
    }

    @Nullable
    public String readString(@NonNull String key) {
        return prefRead().getString(key, "");
    }

    //endregion
}
