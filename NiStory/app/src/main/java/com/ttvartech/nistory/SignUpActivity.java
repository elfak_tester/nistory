package com.ttvartech.nistory;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ttvartech.nistory.Util.AuthorizationManager;

public class SignUpActivity extends AppCompatActivity {

    //Sign up
    private ImageView mAddUserImageImageView;
    private EditText mUserFirstNameEditText;
    private EditText mUserLastNameEditText;
    private EditText mUserPhoneNumberEditText;
    private EditText mUserSignUpEmailEditText;
    private EditText mUserSignUpPasswordEditText;
    private View mProgressView;
    private LinearLayout mUserSignUpFormLinearLayout;

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    FirebaseUser currentUser;

    //Constants
    private static final int ADD_USER_IMAGE = 1;

    private boolean PHOTO_EXISTS = false;

    private Uri mSelectedUserImageUri;
    private Uri mFirebaseUserImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Finding views in activity_sign_up
        mAddUserImageImageView = findViewById(R.id.add_user_image_image_view);
        mUserFirstNameEditText = findViewById(R.id.user_first_name_edit_text);
        mUserLastNameEditText = findViewById(R.id.user_last_name_edit_text);
        mUserPhoneNumberEditText = findViewById(R.id.user_phone_number_edit_text);
        mUserSignUpEmailEditText = findViewById(R.id.user_sign_up_email_edit_text);
        mUserSignUpPasswordEditText = findViewById(R.id.user_sign_up_password_edit_text);
        mProgressView = findViewById(R.id.user_image_upload_progress_bar);
        mUserSignUpFormLinearLayout = findViewById(R.id.user_sign_up_form_linear_layout);

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

    }

    public void selectUserImage(View view) {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ADD_USER_IMAGE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_USER_IMAGE) {
            if (resultCode == RESULT_OK) {

                PHOTO_EXISTS = true;

                mSelectedUserImageUri = data.getData();

                mAddUserImageImageView.setPadding(0, 0, 0, 0);
                Glide.with(getApplicationContext()).load(mSelectedUserImageUri).apply(RequestOptions.circleCropTransform()).into(mAddUserImageImageView);


            } else if (resultCode == RESULT_CANCELED) {

                PHOTO_EXISTS = false;

                mAddUserImageImageView.setPadding(16, 16, 16, 16);
                mAddUserImageImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_outline_photo_camera_24px));
                //Glide.with(this).load(R.drawable.ic_outline_photo_camera_24px).into(mAddUserImageImageView);
            }
        }
    }

    public void tryToSignUp(View view) {

        if (!validateForm()) {
            return;
        }

        mProgressView.setVisibility(View.VISIBLE);
        mUserSignUpFormLinearLayout.setVisibility(View.GONE);

        if (PHOTO_EXISTS)
            createUserWithPhoto();
        else
            createUserWithoutPhoto();

    }

    private boolean validateForm() {

        boolean valid = true;

        String firstName = mUserFirstNameEditText.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            mUserFirstNameEditText.setError("Required.");
            valid = false;
        } else {
            mUserFirstNameEditText.setError(null);
        }

        String lastName = mUserLastNameEditText.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            mUserLastNameEditText.setError("Required.");
            valid = false;
        } else {
            mUserLastNameEditText.setError(null);
        }

        String phoneNumber = mUserPhoneNumberEditText.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mUserPhoneNumberEditText.setError("Required.");
            valid = false;
        } else {
            mUserPhoneNumberEditText.setError(null);
        }

        String email = mUserSignUpEmailEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mUserSignUpEmailEditText.setError("Required.");
            valid = false;
        } else {
            mUserSignUpEmailEditText.setError(null);
        }


        String password = mUserSignUpPasswordEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mUserSignUpPasswordEditText.setError("Required.");
            valid = false;
        } else {
            mUserSignUpPasswordEditText.setError(null);
        }

        if (!PHOTO_EXISTS){
            valid = false;
            Toast.makeText(this, "Please add your profile photo", Toast.LENGTH_LONG).show();
        }

        return valid;

    }

    private void createUserWithPhoto() {

        createAccount();

    }

    private void createUserWithoutPhoto() {

        createAccount();

    }

    private void storePhotoInDatabase() {

        mStorageRef = FirebaseStorage.getInstance().getReference();


        StorageReference storageReference = mStorageRef.child("user_profile_photos/" + AuthorizationManager.getUserUid());

       storageReference.putFile(mSelectedUserImageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        mFirebaseUserImageUri = taskSnapshot.getDownloadUrl();

                        Toast.makeText(SignUpActivity.this, "Photo uploaded.",
                                Toast.LENGTH_SHORT).show();

                        createUserProfile();

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Toast.makeText(SignUpActivity.this, "Photo didn't uploaded.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void createAccount() {

        String email = mUserSignUpEmailEditText.getText().toString();
        String password = mUserSignUpPasswordEditText.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)

                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override

                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            currentUser = mAuth.getCurrentUser();

                            //UserToken.USER_UID = currentUser.getUid();
                            AuthorizationManager.setUserUid(currentUser.getUid());

                            if (PHOTO_EXISTS)
                                storePhotoInDatabase();
                            else
                                createUserProfile();


                            AuthorizationManager.setUserName(email);
                            AuthorizationManager.setUserPassword(password);

                            Toast.makeText(SignUpActivity.this, "Authentication succeeded",
                                    Toast.LENGTH_SHORT).show();


                        } else {

                            Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                            mProgressView.setVisibility(View.GONE);
                            mUserSignUpFormLinearLayout.setVisibility(View.VISIBLE);

                            finish();

                        }

                    }

                });

    }

    private void createUserProfile() {

        String firstName = mUserFirstNameEditText.getText().toString();
        String lastName = mUserLastNameEditText.getText().toString();
        String phoneNumber = mUserPhoneNumberEditText.getText().toString();
        String email = mUserSignUpEmailEditText.getText().toString();

        User user;
        UserProfileChangeRequest profileUpdates;

        if (PHOTO_EXISTS)
        {
            user = new User(firstName, lastName, phoneNumber, email, mFirebaseUserImageUri.toString());
            profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(firstName + " " + lastName)
                    .setPhotoUri(Uri.parse(mFirebaseUserImageUri.toString()))
                    .build();
        }
        else
        {
            user = new User(firstName, lastName, phoneNumber, email, null);
            profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(firstName + " " + lastName)
                    .build();
        }


        mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).setValue(user);



        currentUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {



                        }
                    }
                });

        mProgressView.setVisibility(View.GONE);

        Toast.makeText(SignUpActivity.this, "User created", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();

    }


    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

}
