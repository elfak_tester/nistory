package com.ttvartech.nistory;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Monument implements Serializable {

    private String mMonumentId;
    private String mMonumentName;
    private String mMonumentDescription;
    private String mMonumentImageUrl_1;
    private int mMonumentHearts;
    private double mMonumentLatitude;
    private double mMonumentLongitude;
    private boolean mIsVisited;
    private String mMonumentQuestion;
    private String mMonumentAnswer;

    public Monument() {
    }

    public Monument(String mMonumentName, String mMonumentDescription, String mMonumentImageUrl_1, int mMonumentHearts, double mMonumentLatitude, double mMonumentLongitude) {
        this.mMonumentName = mMonumentName;
        this.mMonumentDescription = mMonumentDescription;
        this.mMonumentImageUrl_1 = mMonumentImageUrl_1;
        this.mMonumentHearts = mMonumentHearts;
        this.mMonumentLatitude = mMonumentLatitude;
        this.mMonumentLongitude = mMonumentLongitude;
        this.mIsVisited = false;
    }

    public Monument(String mMonumentName, String mMonumentDescription, String mMonumentImageUrl_1, int mMonumentHearts, double mMonumentLatitude, double mMonumentLongitude, String mMonumentQuestion, String mMonumentAnswer) {
        this.mMonumentName = mMonumentName;
        this.mMonumentDescription = mMonumentDescription;
        this.mMonumentImageUrl_1 = mMonumentImageUrl_1;
        this.mMonumentHearts = mMonumentHearts;
        this.mMonumentLatitude = mMonumentLatitude;
        this.mMonumentLongitude = mMonumentLongitude;
        this.mIsVisited = false;
        this.mMonumentQuestion = mMonumentQuestion;
        this.mMonumentAnswer = mMonumentAnswer;
    }

    public Monument(String mMonumentName, String mMonumentDescription, String mMonumentImageUrl_1, int mMonumentHearts, double mMonumentLatitude, double mMonumentLongitude, boolean visited) {
        this.mMonumentName = mMonumentName;
        this.mMonumentDescription = mMonumentDescription;
        this.mMonumentImageUrl_1 = mMonumentImageUrl_1;
        this.mMonumentHearts = mMonumentHearts;
        this.mMonumentLatitude = mMonumentLatitude;
        this.mMonumentLongitude = mMonumentLongitude;
        this.mIsVisited = visited;
    }

    public String getmMonumentId() {
        return mMonumentId;
    }

    public void setmMonumentId(String mMonumentId) {
        this.mMonumentId = mMonumentId;
    }

    public String getmMonumentName() {
        return mMonumentName;
    }

    public void setmMonumentName(String mMonumentName) {
        this.mMonumentName = mMonumentName;
    }

    public String getmMonumentDescription() {
        return mMonumentDescription;
    }

    public void setmMonumentDescription(String mMonumentDescription) {
        this.mMonumentDescription = mMonumentDescription;
    }

    public String getmMonumentImageUrl_1() {
        return mMonumentImageUrl_1;
    }

    public void setmMonumentImageUrl_1(String mMonumentImageUrl_1) {
        this.mMonumentImageUrl_1 = mMonumentImageUrl_1;
    }

    public int getmMonumentHearts() {
        return mMonumentHearts;
    }

    public void setmMonumentHearts(int mMonumentHearts) {
        this.mMonumentHearts = mMonumentHearts;
    }

    public double getmMonumentLatitude() {
        return mMonumentLatitude;
    }

    public void setmMonumentLatitude(double mMonumentLatitude) {
        this.mMonumentLatitude = mMonumentLatitude;
    }

    public double getmMonumentLongitude() {
        return mMonumentLongitude;
    }

    public void setmMonumentLongitude(double mMonumentLongitude) {
        this.mMonumentLongitude = mMonumentLongitude;
    }


    public String getmMonumentQuestion() {
        return mMonumentQuestion;
    }

    public void setmMonumentQuestion(String mMonumentQuestion) {
        this.mMonumentQuestion = mMonumentQuestion;
    }


    public void setmIsVisited(boolean mIsVisited) {
        this.mIsVisited = mIsVisited;
    }

    public boolean getmIsVisited() {
        return mIsVisited;
    }

    public String getmMonumentAnswer() {
        return mMonumentAnswer;
    }

    public void setmMonumentAnswer(String mMonumentAnswer) {
        this.mMonumentAnswer = mMonumentAnswer;
    }



    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("mMonumentName", mMonumentName);
        result.put("mMonumentDescription", mMonumentDescription);
        result.put("mMonumentImageUrl_1", mMonumentImageUrl_1);
        result.put("mMonumentHearts", mMonumentHearts);
        result.put("mMonumentLatitude", mMonumentLatitude);
        result.put("mMonumentLongitude", mMonumentLongitude);
        result.put("mIsVisited", mIsVisited);
        result.put("mMonumentQuestion", mMonumentQuestion);
        result.put("mMonumentAnswer", mMonumentAnswer);
        return result;
    }

}
