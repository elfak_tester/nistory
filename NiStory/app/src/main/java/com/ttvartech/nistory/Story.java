package com.ttvartech.nistory;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class Story implements Serializable, Comparable<Story> {

    private String mStoryCreatorID;
    private String mStoryID;
    private String mStoryUserImageUrl;
    private String mStoryUserName;
    private String mStoryTime;
    private String mStoryDescription;
    private String mStoryImageUrl;
    private int mStoryHearts;
    private double mStoryLatitude;
    private double mStoryLongitude;


    private ArrayList<String> mStoryUsers = new ArrayList<>();


    private Map<String, String> mUsers = new HashMap<>();

    public Story(String mStoryUserImageUrl, String mStoryUserName, String mStoryTime, String mStoryDescription, String mStoryImageUrl, int mStoryHearts, double mStoryLatitude, double mStoryLongitude) {
        this.mStoryUserImageUrl = mStoryUserImageUrl;
        this.mStoryUserName = mStoryUserName;
        this.mStoryTime = mStoryTime;
        this.mStoryDescription = mStoryDescription;
        this.mStoryImageUrl = mStoryImageUrl;
        this.mStoryHearts = mStoryHearts;
        this.mStoryLatitude = mStoryLatitude;
        this.mStoryLongitude = mStoryLongitude;
    }

    public Story(String mStoryCreatorID, String mStoryID, String mStoryUserImageUrl, String mStoryUserName, String mStoryTime, String mStoryDescription, String mStoryImageUrl, int mStoryHearts, double mStoryLatitude, double mStoryLongitude) {
        this.mStoryCreatorID = mStoryCreatorID;
        this.mStoryID = mStoryID;
        this.mStoryUserImageUrl = mStoryUserImageUrl;
        this.mStoryUserName = mStoryUserName;
        this.mStoryTime = mStoryTime;
        this.mStoryDescription = mStoryDescription;
        this.mStoryImageUrl = mStoryImageUrl;
        this.mStoryHearts = mStoryHearts;
        this.mStoryLatitude = mStoryLatitude;
        this.mStoryLongitude = mStoryLongitude;
    }

    public Story(String mStoryCreatorID, String mStoryID, String mStoryUserImageUrl, String mStoryUserName, String mStoryTime, String mStoryDescription, String mStoryImageUrl, int mStoryHearts, double mStoryLatitude, double mStoryLongitude, Map<String, String> mUsers) {
        this.mStoryCreatorID = mStoryCreatorID;
        this.mStoryID = mStoryID;
        this.mStoryUserImageUrl = mStoryUserImageUrl;
        this.mStoryUserName = mStoryUserName;
        this.mStoryTime = mStoryTime;
        this.mStoryDescription = mStoryDescription;
        this.mStoryImageUrl = mStoryImageUrl;
        this.mStoryHearts = mStoryHearts;
        this.mStoryLatitude = mStoryLatitude;
        this.mStoryLongitude = mStoryLongitude;
        this.mUsers = mUsers;
    }

    public Story() {
    }

    public String getmStoryCreatorID() {
        return mStoryCreatorID;
    }

    public void setmStoryCreatorID(String mStoryCreatorID) {
        this.mStoryCreatorID = mStoryCreatorID;
    }

    public String getmStoryUserImageUrl() {
        return mStoryUserImageUrl;
    }

    public void setmStoryUserImageUrl(String mStoryUserImageUrl) {
        this.mStoryUserImageUrl = mStoryUserImageUrl;
    }

    public String getmStoryUserName() {
        return mStoryUserName;
    }

    public void setmStoryUserName(String mStoryUserName) {
        this.mStoryUserName = mStoryUserName;
    }

    public String getmStoryTime() {
        return mStoryTime;
    }

    public void setmStoryTime(String mStoryTime) {
        this.mStoryTime = mStoryTime;
    }

    public String getmStoryDescription() {
        return mStoryDescription;
    }

    public void setmStoryDescription(String mStoryDescription) {
        this.mStoryDescription = mStoryDescription;
    }

    public String getmStoryImageUrl() {
        return mStoryImageUrl;
    }

    public void setmStoryImageUrl(String mStoryImageUrl) {
        this.mStoryImageUrl = mStoryImageUrl;
    }

    public int getmStoryHearts() {
        return mStoryHearts;
    }

    public void setmStoryHearts(int mStoryHearts) {
        this.mStoryHearts = mStoryHearts;
    }

    public double getmStoryLatitude() {
        return mStoryLatitude;
    }

    public void setmStoryLatitude(double mStoryLatitude) {
        this.mStoryLatitude = mStoryLatitude;
    }

    public double getmStoryLongitude() {
        return mStoryLongitude;
    }

    public void setmStoryLongitude(double mStoryLongitude) {
        this.mStoryLongitude = mStoryLongitude;
    }

    public String getmStoryID() {
        return mStoryID;
    }

    public void setmStoryID(String mStoryID) {
        this.mStoryID = mStoryID;
    }

    public ArrayList<String> getmStoryUsers() {
        return mStoryUsers;
    }

    public void setmStoryUsers(ArrayList<String> mStoryUsers) {
        this.mStoryUsers = mStoryUsers;
    }


    public Map<String, String> getmUsers() {
        return mUsers;
    }

    public void setmUsers(Map<String, String> mUsers) {
        this.mUsers = mUsers;
    }

    @Override
    public int compareTo(Story story) {

        String input1 = this.getmStoryTime();
        String input2 = story.getmStoryTime();

        Date date1 = null;
        Date date2 = null;

        SimpleDateFormat parser = new SimpleDateFormat("yyyyMMdd_HHmmss");
        try {
            date1 = parser.parse(input1);
            date2 = parser.parse(input2);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (date2.compareTo(date1) > 0)
            return 1;
        else if (this.getmStoryTime().equals(story.getmStoryTime()))
            return 0;
        else
            return -1;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("mStoryCreatorID", mStoryCreatorID);
        result.put("mStoryUserImageUrl", mStoryUserImageUrl);
        result.put("mStoryUserName", mStoryUserName);
        result.put("mStoryTime", mStoryTime);
        result.put("mStoryDescription", mStoryDescription);
        result.put("mStoryImageUrl", mStoryImageUrl);
        result.put("mStoryHearts", mStoryHearts);
        result.put("mStoryLatitude", mStoryLatitude);
        result.put("mStoryLongitude", mStoryLongitude);
        result.put("mStoryLongitude", mStoryLongitude);
        result.put("users", mUsers);
        return result;
    }


}
