package com.ttvartech.nistory;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ttvartech.nistory.Util.AuthorizationManager;

public class UserActivity extends AppCompatActivity {

    ImageView userImageImageView;
    TextView userFirstNameTextView;
    TextView userLastNameTextView;
    TextView userPointsTextView;
    RecyclerView userStoriesRecyclerView;

    User user;
    User selectedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        userImageImageView = findViewById(R.id.user_image_image_view);
        userFirstNameTextView = findViewById(R.id.user_first_name_text_view);
        userLastNameTextView = findViewById(R.id.user_last_name_text_view);
        userPointsTextView = findViewById(R.id.user_points_text_view);
        userStoriesRecyclerView = findViewById(R.id.user_stories_recycler_view);

        Intent intent = getIntent();
        selectedUser = (User) intent.getSerializableExtra("selectedUser");

        if(selectedUser != null)
        {

            Glide.with(getApplicationContext()).load(selectedUser.getmUserImageUrl()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.user_no_image)).into(userImageImageView);

            userFirstNameTextView.setText(selectedUser.getmUserFirstName());
            userLastNameTextView.setText(selectedUser.getmUserLastName());

            userPointsTextView.setText("Points: " + selectedUser.getmUserPoints());
        }
        else
        {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("users").child(AuthorizationManager.getUserUid());

            // Read from the database
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    user = dataSnapshot.getValue(User.class);
                    Glide.with(getApplicationContext()).load(user.getmUserImageUrl()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.user_no_image)).into(userImageImageView);

                    userFirstNameTextView.setText(user.getmUserFirstName());
                    userLastNameTextView.setText(user.getmUserLastName());

                    userPointsTextView.setText("Points: " + user.getmUserPoints());

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                }
            });



        }




    }
}
