package com.ttvartech.nistory;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FriendMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    private static ArrayList<User> friendArrayList;

    public ArrayList<String> userFriendsIDArrayList;
    public ArrayList<Bitmap> friendsImageBitmapArrayList;
    private static boolean clicked = false;


    double myRadius = 30;
    private FusedLocationProviderClient mFusedLocationClient;
    private LatLng myPosition;
    private double myLatitude = 43.333033123456; //43.333621, 21.890711 43.333033, 21.892597
    private double myLongitude = 21.892597123456;
    List<Marker> markersArrayList = new ArrayList<>();



    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private Location myCurrentLocation;
    private double myLastKnownLocLat = 43.333033;
    private double myLastKnownLocLon = 21.892597;
    private DatabaseReference mDatabaseRef;
    private boolean mLocationPermissionGranted = false;
    private static Marker userMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_maps);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        getLocationPermission();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = createLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    //Toast.makeText(getApplicationContext(), "Nova lokacija:" + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();

                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLatitude").setValue(location.getLatitude());
                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLongitude").setValue(location.getLongitude());


                    myLastKnownLocLat = location.getLatitude();
                    myLastKnownLocLon = location.getLongitude();

                    myLatitude = location.getLatitude();
                    myLongitude = location.getLongitude();



                }

            }
        };


        userFriendsIDArrayList = new ArrayList<>();
        friendArrayList = new ArrayList<>();
        friendsImageBitmapArrayList = new ArrayList<>();

    }


    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    public void showMyLocation() {

        userMarker.remove();

        LatLng latLng1 = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng1);
        markerOptions.title("User: " + latLng1.latitude + " " + latLng1.longitude);
        markerOptions.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        userMarker = mMap.addMarker(markerOptions);
    }

    public void getFriends() {


        //Get datasnapshot at your "users" root node
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("friends");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    String s = postSnapshot.getValue(String.class);
                    userFriendsIDArrayList.add(s);
                }
                getEveFriend();
                //getEveryFriend();

                Log.e("Count ", "" + snapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void getEveFriend() {

        friendArrayList.clear();
        mMap.clear();
        for (final String friend : userFriendsIDArrayList) {


            //Get datasnapshot at your "users" root node
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(friend);

            ref.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    User user = snapshot.getValue(User.class);
                    user.setmUserId(snapshot.getKey());
                    friendArrayList.add(user);
                    addFriendMarkers(user);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    public void getEveryFriend() {

        friendArrayList.clear();
        for (final String friend : userFriendsIDArrayList) {


            //Get datasnapshot at your "users" root node
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(friend);

            ref.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    Log.e("Count ", "" + snapshot.getChildrenCount());

                    User user = snapshot.getValue(User.class);

                    int found = -1;
                    int index = 0;

                    for (User user1 : friendArrayList) {

                        if (user1.getmUserEmail().equals(user.getmUserEmail()))
                            found = index;

                        index++;

                    }

                    if (found < 0)
                        friendArrayList.add(user);
                    else
                        friendArrayList.set(found, user);

                    addFriendMarkers(user);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng myLoc = new LatLng(myLatitude, myLongitude);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        float zoomLevel = (float) 4.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, zoomLevel));

        getFriends();
        updateLocationUI();

    }


    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                getCurrentLocation();
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                //mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            //Log.e("Exception: %s", e.getMessage());
        }
    }

    private void addFriendMarkers(User user) {

        getBmp2(user);


        //ovo je glupost
//        mMap.addCircle(new CircleOptions().center(new LatLng(myLatitude, myLongitude))
//                .strokeColor(getResources().getColor(R.color.colorSecondary)).radius(myRadius));

        //mMap.setOnMarkerClickListener(this);

    }

    private Bitmap getBmp(String imageUrl) {

        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(210, 260, conf);

        Bitmap b;

        if (imageUrl.equals("a@a.com"))
            b = ((BitmapDrawable) getResources().getDrawable(R.drawable.aa)).getBitmap();
        else if (imageUrl.equals("d@d.com"))
            b = ((BitmapDrawable) getResources().getDrawable(R.drawable.dd)).getBitmap();
        else
            b = ((BitmapDrawable) getResources().getDrawable(R.drawable.deadpool)).getBitmap();

        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 200, 200, false);

        Canvas canvas1 = new Canvas(bmp);

// paint defines the text color, stroke width and size
        Paint color = new Paint();
        //color.setTextSize(35);
        // stroke
        color.setStyle(Paint.Style.STROKE);
        color.setStrokeJoin(Paint.Join.ROUND);
        color.setStrokeCap(Paint.Cap.ROUND);
        //color.setColor(Color.parseColor("#A30903"));
        color.setColor(Color.parseColor("#A30903"));
        color.setStrokeWidth(10);

// modify canvas
        canvas1.drawBitmap(bitmapResized, 5, 5, color);

        //canvas1.skew(-300, -300);

        //canvas1.drawText("User Name!", 30, 40, color);

        Path path = new Path();
        path.moveTo(80, 205);
        path.lineTo(105, 250);
        path.lineTo(130, 205);
        path.close();

        Path path1 = new Path();
        path1.moveTo(90, 215);
        path1.lineTo(105, 240);
        path1.lineTo(120, 215);
        path1.close();

        canvas1.drawPath(path, color);
        canvas1.drawPath(path1, color);

        Paint color1 = new Paint();
        color1.setColor(Color.parseColor("#A30903"));
        color1.setStrokeWidth(20);


        canvas1.drawPoint(105, 220, color1);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            canvas1.drawRect(5, 5, 195, 195, color);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas1.drawRoundRect(5, 5, 205, 205, 5, 5, color);
        }

        return bmp;

    }

    public class DownloadImageTask extends AsyncTask<User, Void, Bitmap> {
        Listener listener;
        User user;
        InputStream input;

        public DownloadImageTask(final Listener listener) {
            this.listener = listener;
        }

        @Override
        protected Bitmap doInBackground(User... strings) {
            user = strings[0];


            URL url = null;
            Bitmap myBitmap = null;
            try {
                url = new URL(strings[0].getmUserImageUrl());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                input = connection.getInputStream();
                myBitmap = BitmapFactory.decodeStream(input);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return myBitmap;
        }


        @Override
        protected void onPostExecute(Bitmap result) {
            if (null != result) {
                listener.onImageDownloaded(result, user, input);
            }
        }

    }

    public interface Listener {
        Bitmap onImageDownloaded(final Bitmap bitmap, User user, InputStream inputStream);
    }


    private void getBmp2(User user) {
        new DownloadImageTask(new Listener() {
            @Override
            public Bitmap onImageDownloaded(Bitmap bitmap, User user, InputStream inputStream) {
                Bitmap.Config conf = Bitmap.Config.ARGB_8888;
                final Bitmap bmp = Bitmap.createBitmap(210, 260, conf);
                Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, 200, 200, false);

                // bitmapResized = RotateBitmap(bitmapResized, -90);
                Uri uri = Uri.parse(user.getmUserImageUrl());
                try {
                    bitmapResized = rotateImageIfRequired(FriendMapsActivity.this, bitmapResized, uri, inputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Canvas canvas1 = new Canvas(bmp);

// paint defines the text color, stroke width and size
                Paint color = new Paint();
                //color.setTextSize(35);
                // stroke
                color.setStyle(Paint.Style.STROKE);
                color.setStrokeJoin(Paint.Join.ROUND);
                color.setStrokeCap(Paint.Cap.ROUND);
                //color.setColor(Color.parseColor("#A30903"));
                color.setColor(Color.parseColor("#A30903"));
                color.setStrokeWidth(10);

// modify canvas
                canvas1.drawBitmap(bitmapResized, 5, 5, color);

                //canvas1.skew(-300, -300);

                //canvas1.drawText("User Name!", 30, 40, color);

                Path path = new Path();
                path.moveTo(80, 205);
                path.lineTo(105, 250);
                path.lineTo(130, 205);
                path.close();

                Path path1 = new Path();
                path1.moveTo(90, 215);
                path1.lineTo(105, 240);
                path1.lineTo(120, 215);
                path1.close();

                canvas1.drawPath(path, color);
                canvas1.drawPath(path1, color);

                Paint color1 = new Paint();
                color1.setColor(Color.parseColor("#A30903"));
                color1.setStrokeWidth(20);


                canvas1.drawPoint(105, 220, color1);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    canvas1.drawRect(5, 5, 195, 195, color);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    canvas1.drawRoundRect(5, 5, 205, 205, 5, 5, color);
                }

                //Bitmap bmp2 = RotateBitmap(bmp, -90);

                int position = 0;
                for (Marker marker : markersArrayList) {
                    if (marker.getTag().equals(user.getmUserId())) {
                        marker.remove();
                        markersArrayList.remove(position);
                    }
                    position++;
                }

                LatLng latLng = new LatLng(user.getmUserLatitude(), user.getmUserLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(user.getmUserFirstName());
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bmp));
                markerOptions.snippet(user.getmUserEmail());
                Marker friendMarker = mMap.addMarker(markerOptions);
                friendMarker.setTag(user.getmUserId());
                markersArrayList.add(friendMarker);
                return bmp;
            }
        }).execute(user);
    }


    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage, InputStream inputStream) throws IOException {
        InputStream input = inputStream;
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {

        clicked = true;

        Toast.makeText(this, "Info window clicked", Toast.LENGTH_SHORT).show();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (clicked)
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {

                        User user = postSnapshot.getValue(User.class);

                        if (clicked)
                            if (user.getmUserEmail().equals(marker.getSnippet())) {
                                clicked = false;
                                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                                intent.putExtra("selectedUser", user);
                                startActivity(intent);

                            }

                    }

                //getEveryFriend();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return false;

    }


    private void getCurrentLocation() {
        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            // Getting latitude of the current location
            double latitude = location.getLatitude();
            // Getting longitude of the current location
            double longitude = location.getLongitude();
            // Creating a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);
            myPosition = new LatLng(latitude, longitude);


            //mMap.addMarker(new MarkerOptions().position(myPosition).title("My current position"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));

        }

    }


    boolean mRequestingLocationUpdates = true;

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

}
