package com.ttvartech.nistory;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.util.ArrayList;
import java.util.HashMap;

public class MarkerMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    private boolean mLocationPermissionGranted = false;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationCallback mLocationCallback;

    private LocationRequest mLocationRequest;

    private static int fragment;

    double myRadius = 200;


    ArrayList<Story> allStories;
    ArrayList<Story> filteredStories;
    ArrayList<Monument> filteredMonuments;
    ArrayList<User> allFriends;

    NotificationManager manager;

    double myLatitude;
    double myLongitude;
    double myLastKnownLocLat = 43.333033;
    double myLastKnownLocLon = 21.892597;
    //43.333033, 21.892597

    private static ArrayList<Monument> monumentArrayList;

    private static ArrayList<Location> monumentLocations;

    private DatabaseReference mDatabaseRef;

    private static Marker userMarker;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker_maps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getLocationPermission();

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationRequest = createLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    //Toast.makeText(getApplicationContext(), "Location ovde sam :" + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();

                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLatitude").setValue(location.getLatitude());
                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLongitude").setValue(location.getLongitude());

                    myLatitude = location.getLatitude();
                    myLongitude = location.getLongitude();

                    myLastKnownLocLat = location.getLatitude();
                    myLastKnownLocLon = location.getLongitude();

                    showMyLocation();

                }
            }


        };

        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.cancel(11);

        Intent intent = getIntent();

        fragment = intent.getIntExtra("fragment", -1);

        monumentArrayList = new ArrayList<>();
        monumentLocations = new ArrayList<>();


        switch (fragment) {
            case 0: {

                allStories = new ArrayList<>();
                filteredStories = new ArrayList<>();

                allStories = (ArrayList<Story>) intent.getSerializableExtra("stories");

                break;
            }
            case 1: {

                monumentArrayList = (ArrayList<Monument>) intent.getSerializableExtra("monuments");

                break;
            }
            case 2: {

                allFriends = new ArrayList<>();

                allFriends = (ArrayList<User>) intent.getSerializableExtra("friends");

                break;
            }
            case 3: {


                //TODO: Obrisi ovo :)
                //myCurrentLocation = intent.getParcelableExtra("newestLocation");


                //Log.e("myCurrentLocation", "" + myCurrentLocation);

                break;
            }

        }


// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                //mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            //Log.e("Exception: %s", e.getMessage());
        }
    }

    public void showMyLocation() {
//        if (userMarker != null)
//            userMarker.remove();

        LatLng latLng1 = new LatLng(myLatitude, myLongitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng1);
        markerOptions.title("User: " + latLng1.latitude + " " + latLng1.longitude);
        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        // userMarker = mMap.addMarker(markerOptions);
    }

    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }


    }

    private void getMonumentLocations() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("monuments");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count ", "" + snapshot.getChildrenCount());

                Location location = new Location("");

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Monument monument = postSnapshot.getValue(Monument.class);
                    monumentArrayList.add(monument);
                    location.setLatitude(monument.getmMonumentLatitude());
                    location.setLongitude(monument.getmMonumentLongitude());
                    monumentLocations.add(location);
                }

                addMeAndCloseMonuments();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    private HashMap<Marker, Integer> markerIntegerHashMap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(myLastKnownLocLat, myLastKnownLocLon);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        float zoomLevel = (float) 2.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));

        updateLocationUI();

        switch (fragment) {
            case 0: {
                addStoryMarkers();
                break;
            }
            case 1: {
                addMonumentMarkers();
                break;
            }
            case 2: {
                addFriendMarkers();
                break;
            }
            case 3: {
                //TODO: Obrisi ovo :)
               // getMonumentLocations();
                break;
            }

        }

    }

    private void addStoryMarkers() {
        for (Story story : allStories) {
            LatLng latLng = new LatLng(story.getmStoryLatitude(), story.getmStoryLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(story.getmStoryDescription());
            mMap.addMarker(markerOptions);
        }

    }

    private void addMonumentMarkers() {
        for (Monument monument : monumentArrayList) {
            LatLng latLng = new LatLng(monument.getmMonumentLatitude(), monument.getmMonumentLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(monument.getmMonumentName());
            mMap.addMarker(markerOptions);
        }

        //mMap.setOnMarkerClickListener(this);
    }

    private void addFriendMarkers() {
        for (User user : allFriends) {
            LatLng latLng = new LatLng(user.getmUserLatitude(), user.getmUserLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(user.getmUserFirstName());
            mMap.addMarker(markerOptions);
        }
    }

    private void addMeAndCloseMonuments() {
        LatLng lng = new LatLng(myLatitude, myLongitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(lng);
        markerOptions.title("I am here: " + lng.latitude + " " + lng.longitude);
        mMap.addMarker(markerOptions);
        float zoomLevel = (float) 15.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lng, zoomLevel));

        mMap.addCircle(new CircleOptions().center(lng)
                .strokeColor(getResources().getColor(R.color.colorSecondary)).radius(myRadius));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.marker_maps_menu, menu);
        return true;
    }

    static final int SEARCH_REQUEST = 1;
    static final int SEARCH_MONUMENTS = 2;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search_item) {

            if (fragment == 0) {
                Intent intent = new Intent(MarkerMapsActivity.this, SearchActivity.class);
                intent.putExtra("stories", allStories);
                intent.putExtra("fragment", 1);
                intent.putExtra("myLatitude", myLatitude);
                intent.putExtra("myLongitude", myLongitude);
                startActivityForResult(intent, SEARCH_REQUEST);
            } else {
                Intent intent = new Intent(MarkerMapsActivity.this, SearchActivity.class);
                intent.putExtra("monuments", monumentArrayList);
                intent.putExtra("fragment", 2);
                intent.putExtra("myLatitude", myLatitude);
                intent.putExtra("myLongitude", myLongitude);
                startActivityForResult(intent, SEARCH_MONUMENTS);
            }
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SEARCH_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                filteredStories = (ArrayList<Story>) data.getSerializableExtra("filteredStories");
                myRadius = data.getIntExtra("inputRadius", -1);
                GetLastKnownLocation();
            }
        } else if (requestCode == SEARCH_MONUMENTS) {
            if (resultCode == RESULT_OK) {
                filteredMonuments = (ArrayList<Monument>) data.getSerializableExtra("filteredMonuments");
                myRadius = data.getIntExtra("inputRadius", -1);
                //addMeAndCloseMonuments();
                showFilteredMonuments();
            }
        }
    }

    public void GetLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                            myLastKnownLocLat = location.getLatitude();
                            myLastKnownLocLon = location.getLongitude();

                            showFilteredStories();
                        }
                    }
                });
    }

    private void showFilteredStories() {

        mMap.clear();

        if (filteredStories != null)
            for (Story story : filteredStories) {
                LatLng latLng = new LatLng(story.getmStoryLatitude(), story.getmStoryLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(story.getmStoryDescription());
                mMap.addMarker(markerOptions);
            }
        float zoomLevel = (float) 15.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLatitude, myLongitude), zoomLevel));
        mMap.addCircle(new CircleOptions().center(new LatLng(myLastKnownLocLat, myLastKnownLocLon))
                .strokeColor(getResources().getColor(R.color.colorPrimaryDark)).radius(myRadius));
    }

    private void showFilteredMonuments() {

        mMap.clear();

        if (filteredMonuments != null)
            for (Monument monument : filteredMonuments) {
                LatLng latLng = new LatLng(monument.getmMonumentLatitude(), monument.getmMonumentLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                mMap.addMarker(markerOptions);
            }

        float zoomLevel = (float) 15.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLatitude, myLongitude), zoomLevel));
        mMap.addCircle(new CircleOptions().center(new LatLng(myLatitude, myLongitude))
                .strokeColor(getResources().getColor(R.color.colorSecondary)).radius(myRadius));
    }


    // TODO: Obrisi, ovo ti ne treba :)
    @Override
    public boolean onMarkerClick(final Marker marker) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("monuments");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {

                    Monument monument = postSnapshot.getValue(Monument.class);

                    if(monument.getmMonumentName().equals(marker.getTitle()))
                    {
                        Intent intent = new Intent(getApplicationContext(), MonumentActivity.class);
                        intent.putExtra("selectedMonument", monument);
                        startActivity(intent);
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return false;
    }

    boolean mRequestingLocationUpdates = true;

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }



}
