package com.ttvartech.nistory;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class User implements Serializable, Comparable<User> {
    private String mUserId;
    private String mUserFirstName;
    private String mUserLastName;
    private String mUserPhoneNumber;
    private String mUserEmail;
    private String mUserImageUrl;
    private double mUserLatitude;
    private double mUserLongitude;
    private int mUserPoints;
    private ArrayList<Monument> mMonuments = new ArrayList<>();


    public User(String mUserFirstName, String mUserLastName, String mUserPhoneNumber, String mUserEmail, String mUserImageUrl) {
        this.mUserFirstName = mUserFirstName;
        this.mUserLastName = mUserLastName;
        this.mUserPhoneNumber = mUserPhoneNumber;
        this.mUserEmail = mUserEmail;
        this.mUserImageUrl = mUserImageUrl;
        this.mUserPoints = 0;

        this.mUserLatitude = 0.0;
        this.mUserLongitude = 0.0;
    }

    public User(String mUserFirstName, String mUserLastName, String mUserPhoneNumber, String mUserEmail, String mUserImageUrl, double mUserLatitude, double mUserLongitude, int mUserPoints) {
        this.mUserFirstName = mUserFirstName;
        this.mUserLastName = mUserLastName;
        this.mUserPhoneNumber = mUserPhoneNumber;
        this.mUserEmail = mUserEmail;
        this.mUserImageUrl = mUserImageUrl;
        this.mUserLatitude = mUserLatitude;
        this.mUserLongitude = mUserLongitude;
        this.mUserPoints = mUserPoints;
    }

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public String getmUserFirstName() {
        return mUserFirstName;
    }

    public void setmUserFirstName(String mUserFirstName) {
        this.mUserFirstName = mUserFirstName;
    }

    public String getmUserLastName() {
        return mUserLastName;
    }

    public void setmUserLastName(String mUserLastName) {
        this.mUserLastName = mUserLastName;
    }

    public String getmUserPhoneNumber() {
        return mUserPhoneNumber;
    }

    public void setmUserPhoneNumber(String mUserPhoneNumber) {
        this.mUserPhoneNumber = mUserPhoneNumber;
    }

    public String getmUserEmail() {
        return mUserEmail;
    }

    public void setmUserEmail(String mUserEmail) {
        this.mUserEmail = mUserEmail;
    }

    public String getmUserImageUrl() {
        return mUserImageUrl;
    }

    public void setmUserImageUrl(String mUserImageUrl) {
        this.mUserImageUrl = mUserImageUrl;
    }

    public int getmUserPoints() {
        return mUserPoints;
    }

    public void setmUserPoints(int mUserPoints) {
        this.mUserPoints = mUserPoints;
    }

    public double getmUserLatitude() {
        return mUserLatitude;
    }

    public void setmUserLatitude(double mUserLatitude) {
        this.mUserLatitude = mUserLatitude;
    }

    public double getmUserLongitude() {
        return mUserLongitude;
    }

    public void setmUserLongitude(double mUserLongitude) {
        this.mUserLongitude = mUserLongitude;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    @Override
    public int compareTo(User user){
        if (this.getmUserPoints() > user.getmUserPoints())
            return 1;
        else if (this.getmUserPoints() == user.getmUserPoints())
            return 0;
        else
            return -1;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("mUserFirstName", mUserFirstName);
        result.put("mUserLastName", mUserLastName);
        result.put("mUserPhoneNumber", mUserPhoneNumber);
        result.put("mUserEmail", mUserEmail);
        result.put("mUserImageUrl", mUserImageUrl);
        result.put("mUserLatitude", mUserLatitude);
        result.put("mUserLongitude", mUserLongitude);
        result.put("mUserPoints", mUserPoints);
        return result;
    }

}
