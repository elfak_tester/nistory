package com.ttvartech.nistory;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    ArrayList<Story> allStories = new ArrayList<>();
    ArrayList<Story> filteredStories = new ArrayList<>();
    ArrayList<Monument> allMonuments = new ArrayList<>();
    ArrayList<Monument> filteredMonuments = new ArrayList<>();
    private double myLatitude; //43.333621, 21.890711 43.333033, 21.892597
    private double myLongitude;
    int fragment;
    private int radius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();
        fragment = intent.getIntExtra("fragment", -1);
        if (fragment == 1) {
            allStories = (ArrayList<Story>) intent.getSerializableExtra("stories");
            myLatitude = intent.getDoubleExtra("myLatitude", 0);
            myLongitude = intent.getDoubleExtra("myLongitude", 0);
        } else {
            allMonuments = (ArrayList<Monument>) intent.getSerializableExtra("monuments");
            myLatitude = intent.getDoubleExtra("myLatitude", 0);
            myLongitude = intent.getDoubleExtra("myLongitude", 0);
        }

    }

    public void search(View view) {
        EditText editTextRadius = findViewById(R.id.radius_edit_text);
        radius = Integer.parseInt(editTextRadius.getText().toString());

        if (fragment == 1) {
            findStories();
        } else {
            findMonuments();
        }

    }

    private void findMonuments() {
        Location locationA = new Location("");
        locationA.setLatitude(myLatitude);
        locationA.setLongitude(myLongitude);

        for (Monument monument : allMonuments) {
            Location locationB = new Location("");
            locationB.setLatitude(monument.getmMonumentLatitude());
            locationB.setLongitude(monument.getmMonumentLongitude());

            if (locationA.distanceTo(locationB) < radius)
                filteredMonuments.add(monument);
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("filteredMonuments", filteredMonuments);
        returnIntent.putExtra("inputRadius", radius);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void findStories() {

        Location locationA = new Location("");
        locationA.setLatitude(myLatitude);
        locationA.setLongitude(myLongitude);
        for (Story story : allStories) {
            Location locationB = new Location("");
            locationB.setLatitude(story.getmStoryLatitude());
            locationB.setLongitude(story.getmStoryLongitude());

            if (locationA.distanceTo(locationB) < radius)
                filteredStories.add(story);
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("filteredStories", filteredStories);
        returnIntent.putExtra("inputRadius", radius);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private double Haversine(double startLat, double startLong, double endLat, double endLong){

        int EARTH_RADIUS = 6371;

        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(dLong / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c;

    }

}
